import socket, time, datetime, random, string, json, urllib2, requests

from bs4 import BeautifulSoup

def identify():
    ircsock.send("PRIVMSG NickServ :IDENTIFY %s \n" % password)

def ping():
    ircsock.send("PONG :pingis\n")

def sendmsg(chan , msg):
    ircsock.send("PRIVMSG "+ chan +" :"+ msg +"\r\n")

def joinchan(chan):
	ircsock.send("JOIN " + chan + " " + key + "\n")

def state_time():
    currenttime = time.strftime("%I:%M:%S")
    sendmsg(channel, "The current time is " + currenttime)

def fortune_choices(x):
    return {
            0: "\x02IT IS CERTAIN\x02",
            1: "\x02IT IS DECIDEDLY SO\x02",
            2: "\x02WITHOUT A DOUBT\x02",
            3: "\x02YES DEFINITELY\x02",
            4: "\x02YOU MAY RELY ON IT\x02",
            5: "\x02AS I SEE IT, YES\x02",
            6: "\x02MOST LIKELY\x02",
            7: "\x02OUTLOOK GOOD\x02",
            8: "\x02YES\x02",
            9: "\x02SIGNS POINT TO YES\x02",
            10:"\x02REPLY HAZY TRY AGAIN\x02",
            11:"\x02ASK AGAIN LATER\x02",
            12:"\x02BETTER NOT TELL YOU NOW\x02",
            13:"\x02CANNOT PREDICT NOW\x02",
            14:"\x02CONCENTRATE AND ASK AGAIN\x02",
            15:"\x02DON'T COUNT ON IT\x02",
            16:"\x02MY REPLY IS NO\x02",
            17:"\x02MY SOURCES SAY NO\x02",
            18:"\x02OUTLOOK NOT SO GOOD\x02",
            19:"\x02VERY DOUBTFUL\x02",

    }[x]

def fortune(question):

    someint = random.randrange(0,19)

    sendmsg(channel, "Concerning '" + question + "':")
    sendmsg(channel, fortune_choices(someint))

def roll_die(x, y):

    numdie = x
    numsides = y
    rollreturn = ""
    for num in range(0,x):
        temp = random.randrange(1,y + 1)
        rollreturn += "\x02" + str(temp) + "\x02" + " "

    sendmsg(channel, rollreturn)
    
def create_json_data(some_url):
	
	json_data = urllib2.urlopen(some_url)
	data = json.load(json_data)
	
	return data


server = "irc.rizon.net"
channel = "#NukapediaUnofficial"
key = "tomootosoup"
botnick = "TheOracle"
password = "drmanhattan"

ircsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ircsock.connect((server, 6667))
ircsock.send("USER "+ botnick +" "+ botnick +" "+ botnick +" :I AM OMNISCIENT\n") # user authentication
ircsock.send("NICK "+ botnick +"\n")
identify()


while 1:
    ircmsg = ircsock.recv(512)
    ircmsg = ircmsg.strip('\n\r')
    print(ircmsg)

    enter_channel = False

    if "recognized." in ircmsg != -1:
        joinchan(channel)

    if "PING :" in ircmsg != -1:
        ping()

    if "!time" in ircmsg != -1:
        state_time()

    if "!op" in ircmsg != -1:
        nickcheck = ircmsg.split("!")
        if nickcheck[0] != ":AnimeDad":
            sendmsg(channel, "You are not authorized to use that function")

        else:
            nick = ircmsg.split("!op ")
            ircsock.send("MODE "+ channel + " +o " + nick [1] + "\n")
            sendmsg(channel, "Congratulations " + nick [1] + "! You have been opped")

    if ircmsg.find("!voice") != -1:
        nick = ircmsg.split("!voice ")
        ircsock.send("MODE "+ channel + " -o " + nick [1] + "\n")
        ircsock.send("MODE "+ channel + " +v " + nick [1] + "\n")

    if ircmsg.find("!fortune") != -1:
        try:
            somequestion = ircmsg.split("!fortune ")
            fortune(somequestion[1])
        except IndexError:
            sendmsg(channel, "IMPROPER SYNTAX")

    if ircmsg.find("!roll") != -1:
        try:
            someroll = ircmsg.split("!roll ")
            diequery = someroll[1].split("d")
            roll_die(int(diequery[0]), int(diequery[1]))
        except IndexError:
            sendmsg(channel, "IMPROPER SYNTAX")

    if ircmsg.find("PRIVMSG #NukapediaUnofficial") != -1:
        message_parse = ircmsg.split("#NukapediaUnofficial ")
        message_parsetwo = message_parse[1].split(":")

        nick_parse = message_parse[0].split("!")
        nick = nick_parse[0].replace(":","")

        log_time = time.strftime("%I:%M:%S")

        date = datetime.datetime.now()
        datesplit = str(date).split(" ")
        datecheck = False

        logfile = open("replog.txt", "r+")

        for line in logfile:
            if datesplit[0] in line:
                datecheck = True


        if datecheck == False:
            logfile.write("\n" + datesplit[0])
            logfile.write("\n" + "[" + log_time + "] " + "<" + nick + "> " + message_parsetwo[1])
        else:
            logfile.write("\n" + "[" + log_time + "] " + "<" + nick + "> " + message_parsetwo[1])
        logfile.close()

    if ircmsg.find("!sequence") != -1:
        try:
            sequenceparse = ircmsg.split("!sequence ")
            sequence_value = int(sequenceparse[1])
            letters = ""

            for x in range(0,sequence_value):
                letters += random.choice(string.ascii_letters) + " "

            sendmsg(channel, letters)
        except IndexError:
            sendmsg(channel, "IMPROPER SYNTAX")

    if ircmsg.find("PRIVMSG") != -1:

        privparse = ircmsg.split("PRIVMSG")

        if "TheOracle" in privparse[1] and ":AnimeDad" in privparse[0]:
            messagesplit = privparse[1].split(":")
            sendmsg(channel,messagesplit[1])

    if "!fun" in ircmsg != -1:

        fact_roll = random.randrange(0,3)

        if fact_roll == 0:
            query  = requests.get("http://numbersapi.com/random/trivia")
        elif fact_roll == 1:
            query  = requests.get("http://numbersapi.com/random/year")
        elif fact_roll == 2:
            query  = requests.get("http://numbersapi.com/random/math")
        else:
            query  = requests.get("http://numbersapi.com/random/date")

        text_trivia = query.text

        trivia = str(BeautifulSoup(text_trivia))

        sendmsg(channel, trivia)


    if "!food" in ircmsg != -1:

        try:

            message_parse = ircmsg.split("!food ")

            query_split = message_parse[1].split(",")

            name_final = query_split[0].replace(' ', '%20')

            street = query_split[1].replace(' ', '%20')

            locality = query_split[2].replace(' ', '%20')

            api_key = 'fb1cccf8981ba950022935f83038895ad56a3673'

            final_url = 'https://api.locu.com/v1_0/venue/search/?name=' + name_final + '&locality=' + locality + '&street_address=' + street + '&api_key=fb1cccf8981ba950022935f83038895ad56a3673'

            data = create_json_data(final_url)

            x = 0

            while x != 3:

                if len(data['objects']) > 2:

                    for item in data['objects']:
                        restaurant_name = item['name']
                        if item['street_address'] != None and item['postal_code'] != None and item['phone'] != None and x != 3:
                            full_address = item['street_address'] + "\n" + item['locality'] + ", " + item['region'] + " " + item['postal_code']
                            phone = item['phone']
                            sendmsg(channel, restaurant_name)
                            sendmsg(channel, full_address)
                            sendmsg(channel, phone)
                            x += 1

                elif len(data['objects']) > 0 and len(data['objects']) < 3:

                    for item in data['objects']:
                        restaurant_name = item['name']
                        if item['street_address'] != None and item['postal_code'] != None and item['phone'] != None and x != 3:
                            full_address = item['street_address'] + "\n" + item['locality'] + ", " + item['region'] + " " + item['postal_code']
                            phone = item['phone']
                            sendmsg(channel, restaurant_name)
                            sendmsg(channel, full_address)
                            sendmsg(channel, phone)
                            x = 3

                else:
                    sendmsg(channel,"Nothing found")
                    x = 3

        except IndexError:
            print "Please enter the proper format Ex:(Subway,AZ)"

    if "!weather" in ircmsg != -1:
        try:        
            weather_parse = ircmsg.split("!weather ")
            location_parse = weather_parse[1].split(",")
        
            city = location_parse[0]
            state = location_parse[1]
        
            city = city.replace(" ","_")
            
            state = state.replace(" ","_")
        
            weather_url = 'http://api.wunderground.com/api/99393b5432282b35/conditions/q/' + state + '/' + city + '.json'

            weather_data = create_json_data(weather_url)
            
            forecast_url = 'http://api.wunderground.com/api/99393b5432282b35/forecast/q/' + state + '/' + city + '.json'
            
            forecast_data = create_json_data(forecast_url)
        
            temp_f = str(weather_data['current_observation']['temp_f'])
        
            temp_c = str(weather_data['current_observation']['temp_c'])
        
            conditions = str(weather_data['current_observation']['weather'])
            
            forecast = str(forecast_data['forecast']['txt_forecast']['forecastday'][0]["fcttext"])
        
            report = "\x02Fahrenheit:\x02 " + temp_f + " " + "\x02Celsius:\x02 " + temp_c + " " + "\x02Condition:\x02 " + conditions
            
            forecast_report = "\x02Forecast:\x02 " + forecast
         
            sendmsg(channel,report)
            sendmsg(channel,forecast_report)
            
        except KeyError:
			sendmsg(channel,"Invalid weather data")
        except IndexError:
			sendmsg(channel,"Try again")
			
    if "!shitpost" in ircmsg != -1:

        s4s_url = 'http://a.4cdn.org/s4s/catalog.json'

        data = create_json_data(s4s_url)

        x = random.randrange(0,10)

        y = random.randrange(0,10)
        try:
            shitpost = str(data[x]['threads'][y]['com'] + "\n")
            if "<br>" in shitpost: 
				shitpost = shitpost.replace("<br>","\n")
            sendmsg(channel,shitpost)
        except UnicodeEncodeError:
            shitpost = str(data[x + 1]['threads'][y]['com'] + "\n")
            if "<br>" in shitpost: 
				shitpost = shitpost.replace("<br>","\n")
            sendmsg(channel,shitpost)
        except KeyError:
	        shitpost = str(data[x + 1]['threads'][y]['com'] + "\n")
	        sendmsg(channel,shitpost)    
    if "!kick" in ircmsg != -1:
		ircsock.send("KICK #TheSlamJam Spitch" + "\n")   

